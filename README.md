# Golang Lorca Vue Boilerplate

This project created as an example for using Vuejs and golang Lorca together.

The `frontend` directory is a submodule that created by `@vue/cli`.

## Clone
```
git clone --recursive https://framagit.org/hiva-iot/lorca-vue-backend.git
```

## Install
First of all, clone the project and run `go mod tidy`, then run `go build` command. The executable file will be created.

## Frontend
Change directory to frontend by `cd frontend`, then run `npm i` to install dependencies. Then run `npm run build` to build the project.

## Run
```
./lorca-vue
```